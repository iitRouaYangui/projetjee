package iit.tn.JEEProjet.controller;


import iit.tn.JEEProjet.dao.GroupDao;
import iit.tn.JEEProjet.dao.MatiereDao;
import iit.tn.JEEProjet.dao.UserDao;
import iit.tn.JEEProjet.models.Groupe;
import iit.tn.JEEProjet.models.Matiere;
import iit.tn.JEEProjet.models.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * @email Ramesh Fadatare
 */

@WebServlet(urlPatterns = {"/Affectation", "/Affectation/new","/Affectation/insert","/Affectation/delete","/Affectation/edit","/Affectation/update"})
public class AffectationController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private MatiereDao matiereDao;
    private GroupDao groupDao ;
    private UserDao userDao ;

    public void init() {
        matiereDao = new MatiereDao();
        groupDao = new GroupDao() ;
        userDao = new UserDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();

        try {
            switch (action) {
                case "/Affectation/new":
                    showNewForm(request, response);
                    break;
                case "/Affectation/insert":
                    insertAffectation(request, response);
                    break;
                case "/Affectation/delete":
                    deleteAffectation(request, response);
                    break;
                case "/Affectation/edit":
                    showEditForm(request, response);
                    break;
                case "/Affectation/update":
                    updateAffectation(request, response);
                    break;
                default:
                    listAffectation(request, response);
                    break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request,response);
    }
    private void listAffectation(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        List<Matiere> listMatiere = matiereDao.getAllMatieres();

        request.setAttribute("listMatiere", listMatiere);
        RequestDispatcher dispatcher = request.getRequestDispatcher("affectation-matiere.jsp");
        dispatcher.forward(request, response);
    }
    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<User> existingUser = userDao.getAllUser();
        List<Groupe> existingGroupe = groupDao.getAllGroupes();
        RequestDispatcher dispatcher = request.getRequestDispatcher("./affectation-form.jsp");
        request.setAttribute("ens", existingUser);
        request.setAttribute("groupes", existingGroupe);
        dispatcher.forward(request, response);

    }
    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException
    {
        int id = Integer.parseInt(request.getParameter("idMatiere"));
        Matiere existingMatiere = matiereDao.getMatiere(id);
        List<User> existingUser = userDao.getAllUser();
        List<Groupe> existingGroupe = groupDao.getAllGroupes();
        RequestDispatcher dispatcher = request.getRequestDispatcher("./affectation-form.jsp");
        request.setAttribute("matiere", existingMatiere);
        request.setAttribute("ens", existingUser);
        request.setAttribute("groupes", existingGroupe);
        dispatcher.forward(request, response);

    }
    private void insertAffectation(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        String nom  = request.getParameter("nom");
        int id_user = Integer.parseInt(request.getParameter("id_user"));
        int idGroupe = Integer.parseInt(request.getParameter("idGroupe"));
        Groupe groupe=groupDao.getGroupe(idGroupe);
        Matiere newMatiere = new Matiere();
        newMatiere.setId_user(id_user);
        newMatiere.setNom(nom);
        newMatiere.getGroupes().add(groupe);
        groupe.getMatieres().add(newMatiere);
        matiereDao.saveMatiere(newMatiere);
        response.sendRedirect("/JEEProjet_war_exploded/Affectation");
    }

    private void updateAffectation(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("idMatiere"));
        String nom  = request.getParameter("nom");
        int id_user = Integer.parseInt(request.getParameter("id_user"));
        int idGroupe = Integer.parseInt(request.getParameter("idGroupe"));
        Groupe groupe=groupDao.getGroupe(idGroupe);
        Matiere updateMatiere = new Matiere();
        updateMatiere.setIdMatiere(id);
        updateMatiere.setId_user(id_user);
        updateMatiere.setNom(nom);
        updateMatiere.getGroupes().add(groupe);
        groupe.getMatieres().add(updateMatiere);
        matiereDao.updateMatiere(updateMatiere);
        response.sendRedirect("/JEEProjet_war_exploded/Affectation");
    }

    private void deleteAffectation(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("idMatiere"));
        matiereDao.deleteMatiere(id);
        response.sendRedirect("/JEEProjet_war_exploded/Affectation");
    }



}
