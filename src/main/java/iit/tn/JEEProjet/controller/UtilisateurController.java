package iit.tn.JEEProjet.controller;


import iit.tn.JEEProjet.dao.UserDao;
import iit.tn.JEEProjet.models.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


@WebServlet(urlPatterns = {"/Utilisateur", "/Utilisateur/new","/Utilisateur/insert","/Utilisateur/delete","/Utilisateur/edit","/Utilisateur/update"})
public class UtilisateurController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private UserDao userDao;

    public void init() {
        userDao = new UserDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();

        try {
            switch (action) {
                case "/Utilisateur/new":
                    showNewForm(request, response);
                    break;
                case "/Utilisateur/insert":
                    insertUser(request, response);
                    break;
                case "/Utilisateur/delete":
                    deleteUser(request, response);
                    break;
                case "/Utilisateur/edit":
                    showEditForm(request, response);
                    break;
                case "/Utilisateur/update":
                    updateUser(request, response);
                    break;
                default:
                    listUser(request, response);
                    break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request,response);
    }

    private void listUser(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        List<User> listUser = userDao.getAllUser();
        request.setAttribute("listUser", listUser);
        RequestDispatcher dispatcher = request.getRequestDispatcher("utilisateur.jsp");
        dispatcher.forward(request, response);
    }
    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("./user-form.jsp");
        dispatcher.forward(request, response);

    }
    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException
    {
        int id = Integer.parseInt(request.getParameter("id_user"));
        User existingUser = userDao.getUser(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("./user-form.jsp");
        request.setAttribute("user", existingUser);
        dispatcher.forward(request, response);

    }
    private void insertUser(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        String name = request.getParameter("name");
        String underName = request.getParameter("underName");
        String login = request.getParameter("login");
        String pwd = request.getParameter("pwd");
        String role = request.getParameter("role");
        User newUser = new User();
        newUser.setLogin(login);
        newUser.setName(name);
        newUser.setPwd(pwd);
        newUser.setRole(role);
        newUser.setUnderName(underName);
        userDao.saveUser(newUser);
        response.sendRedirect("/JEEProjet_war_exploded/Utilisateur");
    }

    private void updateUser(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id_user"));
        String name = request.getParameter("name");
        String underName = request.getParameter("underName");
        String login = request.getParameter("login");
        String pwd = request.getParameter("pwd");
        String role = request.getParameter("role");
        User updateuser = new User();
        updateuser.setLogin(login);
        updateuser.setName(name);
        updateuser.setPwd(pwd);
        updateuser.setRole(role);
        updateuser.setId_user(id);
        updateuser.setUnderName(underName);
        userDao.updateUser(updateuser);
        response.sendRedirect("/JEEProjet_war_exploded/Utilisateur");
    }

    private void deleteUser(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id_user"));
        userDao.deleteUser(id);
        response.sendRedirect("/JEEProjet_war_exploded/Utilisateur");
    }


}
