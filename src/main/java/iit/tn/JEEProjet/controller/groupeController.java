package iit.tn.JEEProjet.controller;


import iit.tn.JEEProjet.dao.GroupDao;
import iit.tn.JEEProjet.dao.MatiereDao;
import iit.tn.JEEProjet.models.Groupe;
import iit.tn.JEEProjet.models.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * @email Ramesh Fadatare
 */

@WebServlet(urlPatterns = {"/Groupe", "/Groupe/new","/Groupe/insert","/Groupe/delete","/Groupe/edit","/Groupe/update"})
public class groupeController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private GroupDao groupDao ;

    public void init() {
        groupDao = new GroupDao() ;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();

        try {
            switch (action) {
                case "/Groupe/new":
                    showNewForm(request, response);
                    break;
                case "/Groupe/insert":
                    insertGroupe(request, response);
                    break;
                case "/Groupe/delete":
                    deleteUser(request, response);
                    break;
                case "/Groupe/edit":
                    showEditForm(request, response);
                    break;
                case "/Groupe/update":
                    updateGroupe(request, response);
                    break;
                default:
                    listGroupe(request, response);
                    break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request,response);
    }
    private void listGroupe(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        List<Groupe> listGroupe = groupDao.getAllGroupes();
        request.setAttribute("listGroupe", listGroupe);
        RequestDispatcher dispatcher = request.getRequestDispatcher("groupe.jsp");
        dispatcher.forward(request, response);
    }
    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("./groupe-form.jsp");
        dispatcher.forward(request, response);

    }
    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException
    {
        int id = Integer.parseInt(request.getParameter("idGroupe"));
        Groupe existingGroupe = groupDao.getGroupe(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("./groupe-form.jsp");
        request.setAttribute("groupe", existingGroupe);
        dispatcher.forward(request, response);

    }
    private void insertGroupe(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        String name_groupe = request.getParameter("name_groupe");
        int nbr_etudiant =  Integer.parseInt(request.getParameter("nbr_etudiant"));
        Groupe newGroupe = new Groupe();
        newGroupe.setName_groupe(name_groupe);
        newGroupe.setNbrEtudiant(nbr_etudiant);
        groupDao.saveGroupe(newGroupe);
        response.sendRedirect("/JEEProjet_war_exploded/Groupe");
    }

    private void updateGroupe(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("idGroupe"));
        String name_groupe = request.getParameter("name_groupe");
        int nbr_etudiant =  Integer.parseInt(request.getParameter("nbr_etudiant"));
        Groupe updateGroupe = new Groupe();
        updateGroupe.setIdGroupe(id);
        updateGroupe.setName_groupe(name_groupe);
        updateGroupe.setNbrEtudiant(nbr_etudiant);
        groupDao.updateGroupe(updateGroupe);
        response.sendRedirect("/JEEProjet_war_exploded/Groupe");
    }

    private void deleteUser(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("idGroupe"));
        groupDao.deleteGroupe(id);
        response.sendRedirect("/JEEProjet_war_exploded/Groupe");
    }


}
