package iit.tn.JEEProjet.dao;


import org.hibernate.Session;
import org.hibernate.Transaction;
import iit.tn.JEEProjet.models.Matiere;
import iit.tn.JEEProjet.utils.HibernateUtil;

import java.util.List;


public class MatiereDao {
    public void saveMatiere(Matiere matiere) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the student object
            session.save(matiere);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void updateMatiere(Matiere matiere) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the student object
            session.update(matiere);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void deleteMatiere(int id) {

        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();

            // Delete a Matiere object
            Matiere matiere = session.get(Matiere.class, id);
            if (matiere != null) {
                session.delete(matiere);
                System.out.println("Matiere is deleted");
            }

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public Matiere getMatiere(int id) {

        Transaction transaction = null;
        Matiere matiere = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an Matiere object
            matiere = session.get(Matiere.class, id);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return matiere;
    }
    public List<Matiere> getAllMatieres () {
        Transaction transaction = null;
        List <Matiere> listMatieres = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an user object

            listMatieres = session.createQuery(" From Matiere").getResultList();

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return listMatieres;
    }
}
