package iit.tn.JEEProjet.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "groupe")
public class Groupe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_groupe", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idGroupe;
    @Column(name = "name_groupe")
    private String name_groupe;
    @Column(name = "nbr_etudiant")
    private int nbrEtudiant;


    @ManyToMany(cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.REFRESH,
            CascadeType.PERSIST
    } , fetch = FetchType.EAGER , targetEntity = Matiere.class)
    @JoinTable(
            name = "matiere_groupe",
            joinColumns = {
                    @JoinColumn(name = "id_groupe" , referencedColumnName = "id_groupe")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "id_matiere" , referencedColumnName ="id_matiere")
            }
    )
    List< Matiere >  matieres =new ArrayList<>();

    public void setIdGroupe(int idGroupe) {
        this.idGroupe = idGroupe;
    }

    public int getIdGroupe() {
        return idGroupe;
    }

    public void setNbrEtudiant(int nbrEtudiant) {
        this.nbrEtudiant = nbrEtudiant;
    }

    public int getNbrEtudiant() {
        return nbrEtudiant;
    }

    public List<Matiere> getMatieres() {
        return matieres;
    }

    public void setMatieres(List<Matiere> matieres) {
        this.matieres = matieres;
    }

    public String getName_groupe() {
        return name_groupe;
    }

    public void setName_groupe(String name_groupe) {
        this.name_groupe = name_groupe;
    }

    @Override
    public String toString() {
        return "Groupe{" +
                "idGroupe=" + idGroupe + '\'' +
                "nbrEtudiant=" + nbrEtudiant + '\'' +
                '}';
    }
}
