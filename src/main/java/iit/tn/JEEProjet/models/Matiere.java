package iit.tn.JEEProjet.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "matiere")
public class Matiere implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_matiere", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idMatiere;

    @Column(name = "nom")
    private String nom;

    @Column(name = "id_user")
    private int id_user ;

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_user" , referencedColumnName = "id_user", insertable = false, updatable = false)
    private User user;

    public List<Groupe> getGroupes() {
        return groupes;
    }

    public void setGroupes(List<Groupe> groupes) {
        this.groupes = groupes;
    }

    @ManyToMany(targetEntity = Tirage.class,cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.REFRESH,
            CascadeType.PERSIST
    },fetch = FetchType.EAGER)
    @JoinTable(
            name = "tirage_matiere",
            joinColumns = {
                    @JoinColumn(name = "id_matiere" , referencedColumnName = "id_matiere")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "id_tirage" , referencedColumnName = "id_tirage")
            }
    )
    List< Tirage > tirages = new ArrayList<>();

    @ManyToMany(targetEntity = Groupe.class, cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.REFRESH,
            CascadeType.PERSIST } , fetch = FetchType.EAGER)
    @JoinTable(
            name = "matiere_groupe",
            joinColumns = {

                    @JoinColumn(name = "id_matiere" , referencedColumnName ="id_matiere")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "id_groupe" , referencedColumnName = "id_groupe")
            }
    )
    private List<Groupe> groupes = new ArrayList<>();

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Tirage> getTirages() {
        return tirages;
    }

    public void setTirages(List<Tirage> tirages) {
        this.tirages = tirages;
    }

    public void setIdMatiere(int idMatiere) {
        this.idMatiere = idMatiere;
    }

    public int getIdMatiere() {
        return idMatiere;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }



    @Override
    public String toString() {
        return "Matiere{" +
                "idMatiere=" + idMatiere + '\'' +
                "nom=" + nom + '\'' +
                '}';
    }

    public Matiere() {
    }
}
