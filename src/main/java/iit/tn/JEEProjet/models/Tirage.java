package iit.tn.JEEProjet.models;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tirage")
public class Tirage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_tirage", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTirage;

    @Column(name = "date_reception")
    private Date dateReception;

    @Column(name = "nbr_copie")
    private int nbrCopie;
    @Column(name = "file_name")
    private String fileName;
    @Column(name = "file_data")
    private String fileData;


    @ManyToMany(targetEntity = Matiere.class, cascade =
            { CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.REFRESH,
            CascadeType.PERSIST },fetch = FetchType.EAGER)
    @JoinTable(
            name = "tirage_matiere",
            joinColumns = {

                    @JoinColumn(name = "id_tirage" , referencedColumnName = "id_tirage")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "id_matiere" , referencedColumnName = "id_matiere")
            }
    )
    private List<Matiere> matieres = new ArrayList<>();

    public List<Matiere> getMatieres() {
        return matieres;
    }

    public void setMatieres(List<Matiere> matieres) {
        this.matieres = matieres;
    }

    public void setIdTirage(int idTirage) {
        this.idTirage = idTirage;
    }

    public int getIdTirage() {
        return idTirage;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setNbrCopie(int nbrCopie) {
        this.nbrCopie = nbrCopie;
    }

    public int getNbrCopie() {
        return nbrCopie;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileData() {
        return fileData;
    }

    public void setFileData(String fileData) {
        this.fileData = fileData;
    }
    @Override
    public String toString() {
        return "Tirage{" +
                "idTirage=" + idTirage + '\'' +
                "dateReception=" + dateReception + '\'' +
                "nbrCopie=" + nbrCopie + '\'' +
                '}';
    }

    public Tirage() {
    }
}
