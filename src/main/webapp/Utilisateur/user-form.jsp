<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 24/05/2022
  Time: 22:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>

<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <title> Home </title>
    <link rel="stylesheet" href="../style.css">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="sidebar">
    <div class="logo-details">
        <i class='bx bxl-c-plus-plus'></i>
        <span class="logo_name">Tirage</span>
    </div>
    <ul class="nav-links">
        <li>
            <a href="#" class="active">
                <i class='bx bx-grid-alt' ></i>
                <span class="links_name">Dashboard</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class='bx bx-box' ></i>
                <span class="links_name">Demande de tirage</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class='bx bx-list-ul' ></i>
                <span class="links_name">Matières</span>
            </a>
        </li>

        <li>
            <a href="#">
                <i class='bx bx-book-alt' ></i>
                <span class="links_name">Matières à imprimer</span>
            </a>
        </li>
        <li>
            <a href="">
                <i class='bx bx-user' ></i>
                <span class="links_name">Utilisateur</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class='bx bx-list-ul' ></i>
                <span class="links_name">Affection Matières </span>
            </a>
        </li>


        <li class="log_out">
            <a href="#">
                <i class='bx bx-log-out'></i>
                <span class="links_name">Log out</span>
            </a>
        </li>
    </ul>
</div>
<section class="home-section">
    <nav>
        <div class="sidebar-button">
            <i class='bx bx-menu sidebarBtn'></i>
            <span class="dashboard">Dashboard</span>
        </div>

    </nav>
    <div class="home-content">
        <div class="card">
            <br>
            <div align="center">
                <c:if test="${user != null}">
                <form action="update" method="post">
                    </c:if>
                    <c:if test="${user == null}">
                    <form action="insert" method="post">
                        </c:if>
                        <table>
                            <div>
                                <h2>
                                    <c:if test="${user != null}">
                                        modifier utilisateur
                                    </c:if>
                                    <c:if test="${user == null}">
                                        Ajouter un nouveau utilisateur
                                    </c:if>
                                </h2>
                            </div>
                            <c:if test="${user != null}">
                                <input type="hidden" name="id_user" value="<c:out value='${user.id_user}' />" />
                            </c:if>
                            <tr>
                                <th>
                                    <br>
                                    nom:
                                </th>
                                <td>
                                    <br>
                                    <input type="text" name="name" size="45"
                                           value="<c:out value='${user.name}' />"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <br>
                                    prenom:
                                </th>
                                <td>
                                    <br>
                                    <input type="text" name="underName" size="45"
                                           value="<c:out value='${user.underName}' />"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <br>
                                    adresse email:  &nbsp
                                </th>
                                <td>
                                    <br>
                                    <input type="text" name="login" size="45"
                                           value="<c:out value='${user.login}' />"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <br>
                                    Mode de passe:  &nbsp
                                </th>
                                <td>
                                    <br>
                                    <input type="text" name="pwd" size="45"
                                           value="<c:out value='${user.pwd}' />"
                                    />
                                </td>
                            </tr>
                            <tr>

                                <th>
                                    <br>
                                    Role :
                                </th>
                                <td>
                                    <br>
                                    <input type="text" name="role" size="45"
                                           value="<c:out value='${user.role}' />"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <br>
                                    <input type="submit" class="btn btn-primary" value="Save" />
                                </td>
                            </tr>
                        </table>
                    </form>
            </div>
        </div>


    </div>



</section>

<script>
    let sidebar = document.querySelector(".sidebar");
    let sidebarBtn = document.querySelector(".sidebarBtn");
    sidebarBtn.onclick = function() {
        sidebar.classList.toggle("active");
        if(sidebar.classList.contains("active")){
            sidebarBtn.classList.replace("bx-menu" ,"bx-menu-alt-right");
        }else
            sidebarBtn.classList.replace("bx-menu-alt-right", "bx-menu");
    }
</script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
</script>
</body>
</html>
