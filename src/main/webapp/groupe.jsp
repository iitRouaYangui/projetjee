<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 25/05/2022
  Time: 18:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <title> Home </title>
    <link rel="stylesheet" href="style.css">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="sidebar">
    <div class="logo-details">
        <i class='bx bxl-c-plus-plus'></i>
        <span class="logo_name">Tirage</span>
    </div>
    <ul class="nav-links">
        <li>
            <a href="#" class="active">
                <i class='bx bx-grid-alt' ></i>
                <span class="links_name">Dashboard</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class='bx bx-box' ></i>
                <span class="links_name">Demande de tirage</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class='bx bx-list-ul' ></i>
                <span class="links_name">Mati�res</span>
            </a>
        </li>

        <li>
            <a href="#">
                <i class='bx bx-book-alt' ></i>
                <span class="links_name">Mati�res � imprimer</span>
            </a>
        </li>
        <li>
            <a href="Utilisateur">
                <i class='bx bx-user' ></i>
                <span class="links_name">Utilisateur</span>
            </a>
        </li>
        <li>
            <a href="affectation">
                <i class='bx bx-list-ul' ></i>
                <span class="links_name">Affection Mati�res </span>
            </a>
        </li>


        <li class="log_out">
            <a href="#">
                <i class='bx bx-log-out'></i>
                <span class="links_name">Log out</span>
            </a>
        </li>
    </ul>
</div>
<section class="home-section">
    <nav>
        <div class="sidebar-button">
            <i class='bx bx-menu sidebarBtn'></i>
            <span class="dashboard">Dashboard</span>
        </div>

    </nav>
    <div class="home-content">
        <div class="card">
            <br>
            <div>
                <button onclick="location.href='/JEEProjet_war_exploded/Groupe/new'"type="button" class="btn btn-primary" class="right">
                    Ajouter groupe
                </button>
            </div>
            <br>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">nom</th>
                    <th scope="col">nombre des etudiant</th>
                    <th scope="col">action</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="groupe" items="${listGroupe}">
                    <tr>
                        <td><c:out value="${groupe.idGroupe}" /></td>
                        <td><c:out value="${groupe.name_groupe}" /></td>
                        <td><c:out value="${groupe.nbrEtudiant}" /></td>
                        <td>
                            <a href="Groupe/edit?idGroupe=<c:out value='${groupe.idGroupe}' />">Edit</a>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="Groupe/delete?idGroupe=<c:out value='${groupe.idGroupe}' />">Delete</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>


    </div>



</section>

<script>
    let sidebar = document.querySelector(".sidebar");
    let sidebarBtn = document.querySelector(".sidebarBtn");
    sidebarBtn.onclick = function() {
        sidebar.classList.toggle("active");
        if(sidebar.classList.contains("active")){
            sidebarBtn.classList.replace("bx-menu" ,"bx-menu-alt-right");
        }else
            sidebarBtn.classList.replace("bx-menu-alt-right", "bx-menu");
    }
</script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
</script>
</body>
</html>

